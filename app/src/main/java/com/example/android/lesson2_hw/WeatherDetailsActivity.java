package com.example.android.lesson2_hw;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class WeatherDetailsActivity extends AppCompatActivity {
    public static final String KEY_ITEM = "item";
    public static final String KEY_DATE = "date";
    public static final String KEY_WITH_WIND = "withWind";
    public static final String KEY_WITH_PRESSURE = "withPressure";
    private static final String KEY_CITY = "city";
    private static final String KEY_WEATHER_DETAILS = "weatherDetails";
    private static final String TAG = WeatherDetailsActivity.class.getSimpleName();

    String result = null;
    String city = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_weather);

//        DetailFragment detailFragment = new DetailFragment();


//        detailFragment.setItemNumber(getIntent().getIntExtra(Intent.EXTRA_TEXT, 0));
//        detailFragment.setDateSelection(getIntent().getIntExtra(KEY_DATE,0));
//        detailFragment.setWithWind(getIntent().getBooleanExtra(KEY_WITH_WIND, true));
//        detailFragment.setWithPressure(getIntent().getBooleanExtra(KEY_WITH_PRESSURE,false));

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ITEM, getIntent().getIntExtra(KEY_ITEM, 0));
        bundle.putInt(KEY_DATE, getIntent().getIntExtra(KEY_DATE, 0));
        bundle.putBoolean(KEY_WITH_WIND, getIntent().getBooleanExtra(KEY_WITH_WIND, true));
        bundle.putBoolean(KEY_WITH_PRESSURE, getIntent().getBooleanExtra(KEY_WITH_PRESSURE, false));

        DetailFragment detailFragment = DetailFragment.getInstance(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.add(R.id.details_fragment, detailFragment);
        fragmentTransaction.commit();


    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        Log.i(TAG, "onSaveInstanceState");
        bundle.putString(KEY_CITY, city);
        bundle.putString(KEY_WEATHER_DETAILS, result);
        super.onSaveInstanceState(bundle);
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
    }

//    @Override
//    protected void onStart() {
//        Log.i(TAG,"onStart");
//        super.onStart();
//    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

}
