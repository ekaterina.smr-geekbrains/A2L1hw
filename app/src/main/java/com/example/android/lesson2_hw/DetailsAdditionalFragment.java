package com.example.android.lesson2_hw;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by kater on 20.03.2018.
 */

public class DetailsAdditionalFragment extends Fragment {

    //    public static final String KEY_WITH_WIND = "withWind";
//    public static final String KEY_WITH_PRESSURE = "withPressure";
/*
    public static DetailsAdditionalFragment getInstance(Bundle bundle) {
        DetailsAdditionalFragment weatherDetailsAdditinalFragment = new DetailsAdditionalFragment();
        weatherDetailsAdditinalFragment.setArguments(bundle);
        return weatherDetailsAdditinalFragment;
    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.details_additional_fragment, container, false);

        TextView additionalDetailsTextView = (TextView) rootView.findViewById(R.id.additional_details_textView);
        additionalDetailsTextView.setText("Test text from child fragment");
        boolean withWind = getArguments().getBoolean(WeatherDetailsActivity.KEY_WITH_WIND);
        boolean withPressure = getArguments().getBoolean(WeatherDetailsActivity.KEY_WITH_PRESSURE);

        int index = getArguments().getInt(WeatherDetailsActivity.KEY_ITEM);
        int date = getArguments().getInt(WeatherDetailsActivity.KEY_DATE);

        String windForecast = withWind ? WeatherDetailsSpec.getWindForecast(getContext(), index, date) : "";
        String pressureForecast = withPressure ? WeatherDetailsSpec.getPressureForecast(getContext(), index, date) : "";

        additionalDetailsTextView.setText(String.format(Locale.getDefault(), "%s%s", windForecast, pressureForecast));

        return rootView;
    }
}
