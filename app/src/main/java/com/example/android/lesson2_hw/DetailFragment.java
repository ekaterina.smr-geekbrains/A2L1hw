package com.example.android.lesson2_hw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by kater on 15.03.2018.
 */

public class DetailFragment extends Fragment {
    private static final String DETAILS_CHILD_FRAGMENT_TAG = "DetailsAdditionalFragment";
    private static final String KEY_CITY = "city";
    private static final String KEY_WEATHER_DETAILS = "weatherDetails";
    String result = null;
    String city = null;
    private TextView cityTextView;
    private ImageView weatherImageView;
    private TextView temperatureTextView;
    private TextView weatherDetailsTextView;
    private Button buttonSend;
    private int itemNumber;
    private boolean withWind;
    private boolean withPressure;
    private int dateSelection;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, weatherDetailsTextView.getText());
            startActivity(intent);
        }
    };

    public static DetailFragment getInstance(Bundle bundle) {
        DetailFragment weatherDetailFragment = new DetailFragment();
        weatherDetailFragment.setArguments(bundle);
        return weatherDetailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);

//        if (savedInstanceState != null){
//            city = savedInstanceState.getString(KEY_CITY);
//            result = savedInstanceState.getString(KEY_WEATHER_DETAILS);
//        } else {
            /*itemNumber = getIntent().getIntExtra(KEY_ITEM, 0);
            dateSelection = getIntent().getIntExtra(KEY_DATE,1);
            withWind = getIntent().getBooleanExtra(KEY_WITH_WIND, true);
            this.withPressure = getIntent().getBooleanExtra(KEY_WITH_PRESSURE, false);*/
//        }

        itemNumber = getArguments().getInt(WeatherDetailsActivity.KEY_ITEM);
        dateSelection = getArguments().getInt(WeatherDetailsActivity.KEY_DATE);
        withWind = getArguments().getBoolean(WeatherDetailsActivity.KEY_WITH_WIND);
        withPressure = getArguments().getBoolean(WeatherDetailsActivity.KEY_WITH_PRESSURE);

        city = WeatherDetailsSpec.getCity(getContext(), itemNumber);
        cityTextView = (TextView) view.findViewById(R.id.city_textView);

        temperatureTextView = (TextView) view.findViewById(R.id.temperature_textView);
        weatherImageView = (ImageView) view.findViewById(R.id.weather_imageView);
        weatherDetailsTextView = (TextView) view.findViewById(R.id.weatherDetails1_textView);

        getForecast(itemNumber, dateSelection, withWind, withPressure);


        FragmentManager childFragmentManager = getChildFragmentManager();
        DetailsAdditionalFragment detailsAdditionalFragment = (DetailsAdditionalFragment) childFragmentManager.findFragmentByTag(DETAILS_CHILD_FRAGMENT_TAG);
        if (detailsAdditionalFragment == null) {
            FragmentTransaction transaction = childFragmentManager.beginTransaction();
            detailsAdditionalFragment = new DetailsAdditionalFragment();
            detailsAdditionalFragment.setArguments(this.getArguments());
            transaction.add(R.id.child_fragment_container, detailsAdditionalFragment, DETAILS_CHILD_FRAGMENT_TAG);
            transaction.commit();
        }

        buttonSend = (Button) view.findViewById(R.id.button_share);
        buttonSend.setOnClickListener(onClickListener);

        return view;
    }

    private void getForecast(int index, int date, boolean withWind, boolean withPressure) {

        String day = WeatherDetailsSpec.getDay(getContext(), date);
        cityTextView.setText(String.format(Locale.getDefault(), "%s %s", city, day));

        weatherImageView.setImageResource(WeatherDetailsSpec.getCloudinessImage(getContext(), index, date));
        weatherImageView.setImageResource(R.drawable.c0);
        temperatureTextView.setText(WeatherDetailsSpec.getTemperature(getContext(), index, date));

//        String windForecast = withWind ? WeatherDetailsSpec.getWindForecast(getContext(),index, date) : "";
//        String pressureForecast = withPressure ? WeatherDetailsSpec.getPressureForecast(getContext(), index, date) : "";

        String cloudiness = WeatherDetailsSpec.getCloudinessDescription(getContext(), index, date);
        String forecast = WeatherDetailsSpec.getWeatherDetails(getContext(), index, date);
        weatherDetailsTextView.setText(String.format(Locale.getDefault(), "%s%s", cloudiness, forecast));
//        weatherDetailsTextView.setText(String.format(Locale.getDefault(), "%s%s%s%s", cloudiness, forecast, windForecast, pressureForecast));

    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString(KEY_CITY, city);
        bundle.putString(KEY_WEATHER_DETAILS, result);
        super.onSaveInstanceState(bundle);
    }

    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }

    public void setWithWind(boolean withWind) {
        this.withWind = withWind;
    }

    public void setWithPressure(boolean withPressure) {
        this.withPressure = withPressure;
    }

    public void setDateSelection(int dateSelection) {
        this.dateSelection = dateSelection;
    }
}
