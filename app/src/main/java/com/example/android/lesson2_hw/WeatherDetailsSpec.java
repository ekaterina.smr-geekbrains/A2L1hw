package com.example.android.lesson2_hw;

import android.content.Context;

/**
 * Created by kater on 22.02.2018.
 */

public class WeatherDetailsSpec {
    static String getWeatherDetails(Context context, int index, int day) {
        String[] weatherDetails = context.getResources().getStringArray(R.array.weatherForecast);
        String[] weatherDetailsByDay = weatherDetails[index].split(";");
        return weatherDetailsByDay[day];
    }

    static String getCity(Context context, int index) {
        String cityList[] = context.getResources().getStringArray(R.array.cityList);
        return cityList[index];
    }

    static String getDay(Context context, int day) {
        String daysList[] = context.getResources().getStringArray(R.array.dateSelection);
        return daysList[day];
    }

    static String getTemperature(Context context, int index, int day) {
        String temperature[] = context.getResources().getStringArray(R.array.temperature);
        String temperatrueByDay[] = temperature[index].split(";");
        return temperatrueByDay[day];
    }

    static String getCloudinessDescription(Context context, int index, int day) {
        String cloudinessForecast[] = context.getResources().getStringArray(R.array.cloudinessForecast);
        String cloudinessForecastByDay[] = cloudinessForecast[index].split(";");
        int cloudinessType = Integer.parseInt(cloudinessForecastByDay[day]);
        String cloudinessTypes[] = context.getResources().getStringArray(R.array.cloudingessTypes);
        return cloudinessTypes[cloudinessType];
    }

    static int getCloudinessImage(Context context, int index, int day) {
        String cloudinessForecast[] = context.getResources().getStringArray(R.array.cloudinessForecast);
        String cloudinessForecastByDay[] = cloudinessForecast[index].split(";");

        switch (cloudinessForecastByDay[day]) {
            case "0":
                return R.drawable.c0;
            case "1":
                return R.drawable.c1;
            case "2":
                return R.drawable.c2;
            case "3":
                return R.drawable.c3;
            case "4":
                return R.drawable.c4;
            case "5":
                return R.drawable.c5;
            default:
        }
        return 0;
    }

    static String getWindForecast(Context context, int index, int day) {
        String windForecast[] = context.getResources().getStringArray(R.array.windForecast);
        String windForecastByDay[] = windForecast[index].split(";");
        return windForecastByDay[day];
    }

    static String getPressureForecast(Context context, int index, int day) {
        String pressureForecast[] = context.getResources().getStringArray(R.array.pressureForecast);
        String pressureForecastByDay[] = pressureForecast[index].split(";");
        return pressureForecastByDay[day];
    }
}
