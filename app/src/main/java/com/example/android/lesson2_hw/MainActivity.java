package com.example.android.lesson2_hw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements MainFragment.CityListListner {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String KEY_ITEM_NUMBER = "itemNumber";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public void onListItemClick(int itemPosition, int date, boolean withWind, boolean withPressure) {
        Intent intent = new Intent(MainActivity.this, WeatherDetailsActivity.class);
//        intent.putExtra(Intent.EXTRA_TEXT,itemPosition);
        intent.putExtra(WeatherDetailsActivity.KEY_ITEM, itemPosition);
        intent.putExtra(WeatherDetailsActivity.KEY_WITH_WIND, withWind);
        intent.putExtra(WeatherDetailsActivity.KEY_WITH_PRESSURE, withPressure);
        intent.putExtra(WeatherDetailsActivity.KEY_DATE, date);
        startActivity(intent);
    }

}


