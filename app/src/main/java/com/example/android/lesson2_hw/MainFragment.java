package com.example.android.lesson2_hw;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by kater on 15.03.2018.
 */

public class MainFragment extends Fragment {

    private SeekBar dateSeekBar;
    private TextView dateTextView;
    private Switch windSwitch;
    private Switch pressureSwitch;

    private CityListListner mainActivity;
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            String date = null;
            switch (progress) {
                case 0:
                    date = getString(R.string.seek_bar_value_today);
                    break;
                case 1:
                    date = getString(R.string.seek_bar_value_tomorrow);
                    break;
                case 2:
                    date = getString(R.string.seek_bar_value_in3days);
                    break;
                case 3:
                    date = getString(R.string.seek_bar_value_inAWeek);
                    break;
                default:
                    break;
            }
            dateTextView.setText(date);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    @Override
    public void onAttach(Context context) {
        mainActivity = (CityListListner) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_fragment, container, false);

        dateSeekBar = rootView.findViewById(R.id.date_seek_bar);
        dateTextView = rootView.findViewById(R.id.date_text_view);
        windSwitch = rootView.findViewById(R.id.wind_switch);
        pressureSwitch = rootView.findViewById(R.id.pressure_switch);

        RecyclerView cityListRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        cityListRecyclerView.setLayoutManager(layoutManager);
        cityListRecyclerView.setAdapter(new MyAdapter(getResources().getStringArray(R.array.cityList)));

        dateSeekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);

        return rootView;
    }

    private void showWeatherDetailsScreen(int cityItem, int date, boolean withWind, boolean withPressure) {
        mainActivity.onListItemClick(cityItem, date, withWind, withPressure);
    }

    interface CityListListner {
        void onListItemClick(int cityItem, int date, boolean withWind, boolean withPressure);
    }

    private class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView cityItemTextView;

        MyViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.city_list_item, parent, false));
            itemView.setOnClickListener(this);
            cityItemTextView = (TextView) itemView.findViewById(R.id.city_list_item_textView);
        }

        void bind(String city) {
            if (getLayoutPosition() != RecyclerView.NO_POSITION) {
                cityItemTextView.setText(city);
            }
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            showWeatherDetailsScreen(position, dateSeekBar.getProgress(), windSwitch.isChecked(), pressureSwitch.isChecked());
        }
    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
        String[] cityList;

        MyAdapter(String[] cityList) {
            this.cityList = cityList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new MyViewHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.bind(cityList[position]);
        }

        @Override
        public int getItemCount() {
            return cityList.length;
        }
    }
}
